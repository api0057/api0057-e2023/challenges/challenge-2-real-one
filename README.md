# Challenge 2 - Mercredi 5 juillet

* Durée : **20 mins.**
* Entraidez-vous.

## Consignes

* Sur votre VM, ouvrez un client `psql` sur la DB que nous vous avons créée cette nuit en tappant:
   ```bash
   psql 'postgres://thisIsAUser:thisIsAPasswordMouhaha@0.0.0.0:6543/thisIsADbForChallenge2'
   ```

Cette base de données contient 2 tables:
* Une table `addresses` qui contient la BAN, la colonne qui vous interessera en particulier est `code_insee` qui correspond au code de la commune.
* Une table `registered` qui contient le nombre d'inscrit⋅es par commune lors de la présidentielle 2022 (!par bureau de vote!), cette table contient trois colonnes en particuliers: `code_insee`, `code_bdv` (identifiant un bureau de vote) et `registered` qui contient le nombre d'inscrit⋅es dans le bureau de vote.

## Question

**Quelles sont les 10 communes avec le ratio nb_inscrits/nb_addresses le plus important ?**
